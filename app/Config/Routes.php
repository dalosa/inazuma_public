<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */

//Rutas sin membresía
//$routes->group('public', static function ($routes) {
        $routes->get('/', 'Homecontroller::index');
        $routes->get('equipos', 'Equipocontroller::tablaequipo');
        $routes->get('sobrenosotros', 'Sobrenosotroscontroller::nosotros');
        $routes->get('contactanos', 'Contactanoscontroller::contacto');

//});

//Para ver los partidos suscribirse
$routes->get('partidos', 'Partidocontroller::tablapartido');


//Funcion login
service('auth')->routes($routes);
