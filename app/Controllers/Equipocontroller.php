<?php

namespace App\Controllers;

use App\Models\Equipomodel;
use App\Models\Entrenadormodel;

class Equipocontroller extends BaseController {
   
    public function tablaequipo() {
        $data['title'] = "Equipos FFI";
        $datos = new Equipomodel();
        $data['equipos'] = $datos->findAll();
        
        $entrenador = new Entrenadormodel();
        $entrenadores = $entrenador->findAll();
        //cambiar estructura array
        foreach ($entrenadores as $entrenador) {
            $listaEntrenadores[$entrenador->Cod_entrenador] = $entrenador->Nombre . ' ' . $entrenador->Apellidos;
        }
        $data['entrenadores'] = $listaEntrenadores;
        return view('inazuma/tablaequipos', $data);
    }

}