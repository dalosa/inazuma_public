<?php

namespace App\Controllers;


class Homecontroller extends BaseController {

    public function index(): string {
        $data['title'] = 'Home';
        return view('inazuma/home', $data);
    }
    
}
