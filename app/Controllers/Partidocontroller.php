<?php

namespace App\Controllers;

use App\Models\Partidomodel;
use App\Models\Equipomodel;
use App\Models\Arbitromodel;

class Partidocontroller extends BaseController {

    public function tablapartido() {
        $data['title'] = "Partidos";
        $datos = new Partidomodel();
        $data['partidos'] = $datos->findAll();
        
        //Llamar a Equipomodel
        $equipo = new Equipomodel();
        $equipos = $equipo->findAll();
        //cambiar estructura array
        foreach ($equipos as $equipo) {
            $listaEquipos[$equipo->Cod_equipo] = $equipo->Nombre;
        }
        
        //Llamar a Arbitromodel
        $arbitro = new Arbitromodel();
        $arbitros = $arbitro->findAll();
        //cambiar estructura array
        foreach ($arbitros as $arbitro) {
            $listaArbitros[$arbitro->Cod_arbitro] = $arbitro->Nombre . ' ' . $arbitro->Apellidos;
        }
        
        $data['arbitros'] = $listaArbitros;
        $data['equipos'] = $listaEquipos;
        return view('inazuma/tablapartidos', $data);
    }
}
