<?php
namespace app\Models;

use CodeIgniter\Model;

class Arbitromodel extends Model{

    protected $table = 'arbitro';
    protected $primaryKey = 'Cod_arbitro';
    protected $returnType = 'object';
    protected $allowedFields = ['Cod_arbitro','Nombre', 'Apellidos'];
}


