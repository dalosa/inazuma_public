<?php
namespace app\Models;

use CodeIgniter\Model;

class Partidomodel extends Model{

    protected $table = 'partido';
    protected $primaryKey = 'Cod_partido';
    protected $returnType = 'object';
    protected $allowedFields = ['Cod_partido','Elocal', 'Evisitante', 'Goles_equipo1', 'Goles_equipo2', 'Fecha', 'Hora', 'Estadio', 'Arbitro', 'Jornada'];
}
