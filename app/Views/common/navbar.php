<!-- navbar -->


<ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
</ul>

<ul class="navbar-nav ml-auto align-items-center">
    <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button"><i class="fas fa-search"></i></a>
    </li>
    <li class="nav-item">
        <a class="nav-link mr-3" data-widget="fullscreen" href="#" role="button"><i class="fas fa-expand-arrows-alt"></i></a>
    </li>
    <li class="nav-item">
        <div class="d-flex align-items-center">
            <div class="info mr-2">
                <?php if (auth()->loggedIn()): ?>
                    <a href="logout" class="nav-link"><?=auth()->user()->username ?></a>
                <?php else: ?>
                    <a href="login" class="nav-link">Login</a>
                <?php endif ?>
            </div>
            <div class="image">
                <img style="width:50px; height:auto" src="assets/images/el_gato.jpeg" class="img-circle elevation-3" alt="User Image">
            </div>
        </div>
    </li>
</ul>

