<!-- Sidebar -->

<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

<div class="sidebar">
<a href="<?= base_url('/') ?>" class="brand-link d-flex justify-content-center">
    <img style="width:105px; height:65px; object-fit: contain; padding: 5px; box-shadow: 5px 5px 5px grey; background-color: #f0f0f0; border: 2px solid #750d0d;" src="assets/images/ffi_logo.png" alt="FFI Logo" class="img-circle elevation-3" style="opacity: .5">
</a>
   

    <div style="padding-top: 10px" class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-sidebar">
                    <i class="fas fa-search fa-fw"></i>
                </button>
            </div>
        </div><div class="sidebar-search-results"><div class="list-group"><a href="#" class="list-group-item"><div class="search-title"><strong class="text-light"></strong>N<strong class="text-light"></strong>o<strong class="text-light"></strong> <strong class="text-light"></strong>e<strong class="text-light"></strong>l<strong class="text-light"></strong>e<strong class="text-light"></strong>m<strong class="text-light"></strong>e<strong class="text-light"></strong>n<strong class="text-light"></strong>t<strong class="text-light"></strong> <strong class="text-light"></strong>f<strong class="text-light"></strong>o<strong class="text-light"></strong>u<strong class="text-light"></strong>n<strong class="text-light"></strong>d<strong class="text-light"></strong>!<strong class="text-light"></strong></div><div class="search-path"></div></a></div></div>
    </div>

    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
                <a href="<?= base_url('/') ?>" class="nav-link">
                    <i class="nav-icon fas fa-home"></i>
                    <p>
                        Home
                        <span class="right badge badge-danger">New</span>
                    </p>
                </a>
            </li>
            

            <li class="nav-item">
                <a href="<?= base_url('partidos') ?>" class="nav-link">
                    <i class="nav-icon fas fa-futbol"></i>
                    <p>
                        Partidos
                        <span class="right badge badge-danger">New</span>
                    </p>
                </a>
            </li>

            
            
            
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-shirt"></i>
                    <p>
                        Equipos
                        <i class="fas fa-angle-left right"></i>
                        <span class="badge badge-info right">8</span>
                    </p>
                </a>
                
                
                
                <ul class="nav nav-treeview">
                    <li style="background-color: #961111; box-shadow: 5px 5px 5px grey; color: grey; border-radius: 10px;" class="nav-item">
                        <a href="<?= base_url('equipos') ?>" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p style=" font-weight: bold;" class="text-light">Todos los equipos</p>
                        </a>
                    </li>
         
                    <li class="nav-item equipo1">
                        <a href="../layout/top-nav-sidebar.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Instituto Raimon</p>
                        </a>
                    </li>
                    <li class="nav-item equipo2">
                        <a href="../layout/boxed.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Royal Academy</p>
                        </a>
                    </li>
                    <li class="nav-item equipo3">
                        <a href="../layout/fixed-sidebar.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Instituto Zeus</p>
                        </a>
                    </li>
                    <li class="nav-item equipo4">
                        <a href="../layout/fixed-sidebar-custom.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Gemini Storm</p>
                        </a>
                    </li>
                    <li class="nav-item equipo5">
                        <a href="../layout/fixed-topnav.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Epsilon</p>
                        </a>
                    </li>
                    <li class="nav-item equipo6">
                        <a href="../layout/fixed-footer.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Diamond Dust</p>
                        </a>
                    </li>
                    <li class="nav-item equipo7">
                        <a href="../layout/collapsed-sidebar.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Prominence</p>
                        </a>
                    </li>
                    <li class="nav-item equipo8">
                        <a href="../layout/collapsed-sidebar.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Genesis</p>
                        </a>
                    </li>  
                </ul>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?= base_url('sobrenosotros') ?>" class="nav-link">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-people-fill ml-1" viewBox="0 0 16 16">
                        <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6m-5.784 6A2.24 2.24 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.3 6.3 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5"/>
                        <p class="ml-2"> Sobre Nosotros</p>
                    </svg>
                </a>
            </li>
            <?php if (auth()->loggedIn() AND auth()->user()->inGroup('admin')): ?>
            <li class="nav-item ">
                <a href="<?= base_url('auth/user') ?>" class="nav-link">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-gear-fill ml-1" viewBox="0 0 16 16">
                        <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
                    </svg>
                    <p class="ml-2"> Administración</p>
                </a>
            </li>
            <?php endif ?>

