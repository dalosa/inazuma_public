<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('title') ?>
<?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sobre Nosotros - Football Frontier International</title>
    <!-- Enlace a Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Estilos personalizados -->
    <style>
        .header-container {
            background-color: #750d0d;
            color: #fff;
            padding: 20px;
            border-radius: 10px;
            margin-bottom: 20px;
        }
        .main-container {
            background-color: #f8f9fa;
            padding: 30px;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            margin-bottom: 20px;
        }
        .team-member {
            background-color: #fff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
    <div class="container">
        <!-- Encabezado -->
        <header class="header-container mt-5">
            <h1 class="text-center">Sobre Nosotros</h1>
        </header>
        
        <!-- Contenido principal -->
        <main class="main-container">
            <div class="row justify-content-center">
    <div class="col-md-6 text-center">
        <h2>Nuestra Historia</h2>
        <p>Somos un equipo apasionado de Inazuma Eleven y desarrolladores web que crearon Football Frontier International con el objetivo de ofrecer a los fans de Inazuma Eleven un lugar donde puedan disfrutar del mundo del fútbol y compartir su pasión.</p>
        <p>Nuestra historia comenzó en 2º de ASIR, cuando unimos nuestras habilidades para crear una plataforma única para todos los aficionados de Inazuma Eleven. Desde entonces, hemos estado comprometidos en proporcionar la mejor experiencia posible a nuestra comunidad.</p>
    </div>
    <div class="col-md-6 text-center">
        <div class="team-member ml-5">
            <img src="assets/images/el_gato.jpeg" style="border-radius: 50%; max-width: 200px;">
            <h3>Ed1</h3>
            <p>Nico y David son apasionados desarrolladores web con experiencia en la creación de sitios web dinámicos y atractivos. Su amor por Inazuma Eleven los llevó a unirse al equipo para contribuir con su experiencia técnica.</p>
        </div>
    </div>
</div>
        </main>
    </div>

    <!-- Scripts de Bootstrap (jQuery necesario) -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
<?= $this->endSection() ?>