<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('title') ?>
    <?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

        <div class="row justify-content-center">
    <div class="col-xl-15">
        <div class="table-responsive">
            <table id="myTable2" class="table rounded-top border-dark text-center">
                <thead>
                    <tr>
                        <th class="rounded-top text-white" colspan="8" style="background-color: #750d0d;">Resultados</th>
                    </tr>
                </thead>
                <?php for($i=1;$i<8;$i++):?>
                    <tbody> 
                        <tr>
                            <th class="rounded-top text-white" colspan="8" style="background-color: #ad1111;">Jornada <?= $i; ?></th>
                        </tr>
                        <?php foreach ($partidos as $partido): ?>
                            <?php if ($partido->Jornada==$i):?>
                                <tr>
                                    <td class="margin-right mr-5 align-middle"><strong><?= date('d/m/Y - H:i', strtotime($partido->Fecha . ' ' . $partido->Hora)) ?></strong></td>
                                    <td class="float-md-right align-middle"><strong><?= $equipos[$partido->Elocal] ?> <img src="assets/images/escudos/0<?= $partido->Elocal ?>.png" width="50px"/></strong></td>
                                    <td class="align-middle"><strong><?= $partido->Goles_local ?></strong></td>
                                    <td class="align-middle"><strong>:</strong></td>
                                    <td class="align-middle"><strong><?= $partido->Goles_visitante ?> </strong></td>
                                    <td class="float-md-left align-middle"><strong><img src="assets/images/escudos/0<?= $partido->Evisitante ?>.png" width="50px"/> <?= $equipos[$partido->Evisitante] ?></strong></td>
                                    <td class="align-middle"><strong><?= $partido->Estadio ?></strong></td>
                                    <td class="align-middle"><strong><?= $arbitros[$partido->Arbitro] ?></strong></td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <!-- Agregar una fila vacía como separador entre jornadas -->
                        <tr><td colspan="8">&nbsp;</td></tr>
                    </tbody>
                <?php endfor; ?>    
            </table>
        </div>
    </div>
</div>

<?= $this->endSection() ?>

<?= $this->include('common/datatables') ?>

