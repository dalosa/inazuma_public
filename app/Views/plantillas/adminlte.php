
<!DOCTYPE html>
<html lang="en" style="height: auto;"><head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title><?= $title ?></title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto&display=swap">
    	<link rel="stylesheet" href="<?= base_url('assets/css/font-awesome/all.min.css') ?>">
    	<link rel="stylesheet" href="<?= base_url('assets/css/adminlte.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('assets/css/dataTables.bootstrap4.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('assets/css/inazuma.css') ?>">
         <style>
        /* Aplicar la fuente "Pacifico" al contenido */
        .content-text {
            font-family: 'Roboto', sans-serif;
        }
        </style>
        <?= $this->renderSection('css') ?>
	<body class="sidebar-mini" style="height: auto;">

    	<div class="wrapper">

        	<nav class="main-header navbar navbar-expand navbar-white navbar-light">
            	       <?= $this->include('common/navbar') ?>
        	</nav>

        	<aside class="main-sidebar sidebar-dark-primary elevation-4">
            	       <?= $this->include('common/sidebar') ?>
        	</aside>

        	<div class="content-wrapper" style="min-height: 1604.8px;">

            	<section class="content-header">
                	<div class="container-fluid">
                    	<?= $this->renderSection('page_title') ?>
                	</div>
            	</section>
               	 
            	<section class="content">
                    <div class="container-fluid content-text">
                    <?= $this->renderSection('content') ?>
                </div>
            	</section>

        	</div>

        	<footer class="main-footer">
            	       <?= $this->include('common/footer') ?>
        	</footer>

    	<script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    	<script src="<?= base_url('assets/js/adminlte.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/dataTables.bootstrap4.min.js') ?>"> </script>
        <?= $this->renderSection('js') ?>
        
        
        
	</body>
</html>

